package org.kathra.resourcemanager.service;

import org.kathra.core.model.BinaryRepository;
import java.util.List;

public interface BinaryRepositoriesService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new binaryrepository
    * 
    * @param binaryrepository BinaryRepository object to be created (required)
    * @return BinaryRepository
    */
    BinaryRepository addBinaryRepository(BinaryRepository binaryrepository) throws Exception;

    /**
    * Delete a registered binaryrepository
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteBinaryRepository(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible binaryrepositories for authenticated user
    * 
    * @return List<BinaryRepository>
    */
    List<BinaryRepository> getBinaryRepositories() throws Exception;

    /**
    * Retrieve a specific binaryrepository object
    * 
    * @param resourceId resource's id (required)
    * @return BinaryRepository
    */
    BinaryRepository getBinaryRepository(String resourceId) throws Exception;

    /**
    * Fully update a registered binaryrepository
    * 
    * @param resourceId resource's id (required)
    * @param binaryrepository BinaryRepository object to be updated (required)
    * @return BinaryRepository
    */
    BinaryRepository updateBinaryRepository(String resourceId, BinaryRepository binaryrepository) throws Exception;

    /**
    * Partially update a registered binaryrepository
    * 
    * @param resourceId resource's id (required)
    * @param binaryrepository BinaryRepository object to be updated (required)
    * @return BinaryRepository
    */
    BinaryRepository updateBinaryRepositoryAttributes(String resourceId, BinaryRepository binaryrepository) throws Exception;

}
