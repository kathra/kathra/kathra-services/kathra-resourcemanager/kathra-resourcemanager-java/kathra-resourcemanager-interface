package org.kathra.resourcemanager.service;

import org.kathra.core.model.Assignation;
import java.util.List;

public interface AssignationsService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new assignation
    * 
    * @param assignation Assignation object to be created (required)
    * @return Assignation
    */
    Assignation addAssignation(Assignation assignation) throws Exception;

    /**
    * Delete a registered assignation
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteAssignation(String resourceId) throws Exception;

    /**
    * Retrieve a specific assignation object
    * 
    * @param resourceId resource's id (required)
    * @return Assignation
    */
    Assignation getAssignation(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible assignations for authenticated user
    * 
    * @return List<Assignation>
    */
    List<Assignation> getAssignations() throws Exception;

    /**
    * Fully update a registered assignation
    * 
    * @param resourceId resource's id (required)
    * @param assignation Assignation object to be updated (required)
    * @return Assignation
    */
    Assignation updateAssignation(String resourceId, Assignation assignation) throws Exception;

    /**
    * Partially update a registered assignation
    * 
    * @param resourceId resource's id (required)
    * @param assignation Assignation object to be updated (required)
    * @return Assignation
    */
    Assignation updateAssignationAttributes(String resourceId, Assignation assignation) throws Exception;

}
