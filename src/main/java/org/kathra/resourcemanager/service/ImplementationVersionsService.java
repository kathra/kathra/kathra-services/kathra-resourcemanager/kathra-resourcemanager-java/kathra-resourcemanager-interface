package org.kathra.resourcemanager.service;

import org.kathra.core.model.ImplementationVersion;
import java.util.List;

public interface ImplementationVersionsService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new implementationversion
    * 
    * @param implementationversion ImplementationVersion object to be created (required)
    * @return ImplementationVersion
    */
    ImplementationVersion addImplementationVersion(ImplementationVersion implementationversion) throws Exception;

    /**
    * Delete a registered implementationversion
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteImplementationVersion(String resourceId) throws Exception;

    /**
    * Retrieve a specific implementationversion object
    * 
    * @param resourceId resource's id (required)
    * @return ImplementationVersion
    */
    ImplementationVersion getImplementationVersion(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible implementationversions for authenticated user
    * 
    * @return List<ImplementationVersion>
    */
    List<ImplementationVersion> getImplementationVersions() throws Exception;

    /**
    * Fully update a registered implementationversion
    * 
    * @param resourceId resource's id (required)
    * @param implementationversion ImplementationVersion object to be updated (required)
    * @return ImplementationVersion
    */
    ImplementationVersion updateImplementationVersion(String resourceId, ImplementationVersion implementationversion) throws Exception;

    /**
    * Partially update a registered implementationversion
    * 
    * @param resourceId resource's id (required)
    * @param implementationversion ImplementationVersion object to be updated (required)
    * @return ImplementationVersion
    */
    ImplementationVersion updateImplementationVersionAttributes(String resourceId, ImplementationVersion implementationversion) throws Exception;

}
