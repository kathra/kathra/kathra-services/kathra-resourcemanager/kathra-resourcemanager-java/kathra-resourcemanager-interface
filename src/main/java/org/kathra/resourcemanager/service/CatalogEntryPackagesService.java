package org.kathra.resourcemanager.service;

import org.kathra.core.model.CatalogEntryPackage;
import java.util.List;

public interface CatalogEntryPackagesService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new catalogentrypackage
    * 
    * @param catalogentrypackage CatalogEntryPackage object to be created (required)
    * @return CatalogEntryPackage
    */
    CatalogEntryPackage addCatalogEntryPackage(CatalogEntryPackage catalogentrypackage) throws Exception;

    /**
    * Delete a registered catalogentrypackage
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteCatalogEntryPackage(String resourceId) throws Exception;

    /**
    * Retrieve a specific catalogentrypackage object
    * 
    * @param resourceId resource's id (required)
    * @return CatalogEntryPackage
    */
    CatalogEntryPackage getCatalogEntryPackage(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible catalogentrypackages for authenticated user
    * 
    * @return List<CatalogEntryPackage>
    */
    List<CatalogEntryPackage> getCatalogEntryPackages() throws Exception;

    /**
    * Fully update a registered catalogentrypackage
    * 
    * @param resourceId resource's id (required)
    * @param catalogentrypackage CatalogEntryPackage object to be updated (required)
    * @return CatalogEntryPackage
    */
    CatalogEntryPackage updateCatalogEntryPackage(String resourceId, CatalogEntryPackage catalogentrypackage) throws Exception;

    /**
    * Partially update a registered catalogentrypackage
    * 
    * @param resourceId resource's id (required)
    * @param catalogentrypackage CatalogEntryPackage object to be updated (required)
    * @return CatalogEntryPackage
    */
    CatalogEntryPackage updateCatalogEntryPackageAttributes(String resourceId, CatalogEntryPackage catalogentrypackage) throws Exception;

}
