package org.kathra.resourcemanager.service;

import org.kathra.core.model.SourceRepository;
import java.util.List;

public interface SourceRepositoriesService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new sourcerepository
    * 
    * @param sourcerepository SourceRepository object to be created (required)
    * @return SourceRepository
    */
    SourceRepository addSourceRepository(SourceRepository sourcerepository) throws Exception;

    /**
    * Delete a registered sourcerepository
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteSourceRepository(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible sourcerepositories for authenticated user
    * 
    * @return List<SourceRepository>
    */
    List<SourceRepository> getSourceRepositories() throws Exception;

    /**
    * Retrieve a specific sourcerepository object
    * 
    * @param resourceId resource's id (required)
    * @return SourceRepository
    */
    SourceRepository getSourceRepository(String resourceId) throws Exception;

    /**
    * Fully update a registered sourcerepository
    * 
    * @param resourceId resource's id (required)
    * @param sourcerepository SourceRepository object to be updated (required)
    * @return SourceRepository
    */
    SourceRepository updateSourceRepository(String resourceId, SourceRepository sourcerepository) throws Exception;

    /**
    * Partially update a registered sourcerepository
    * 
    * @param resourceId resource's id (required)
    * @param sourcerepository SourceRepository object to be updated (required)
    * @return SourceRepository
    */
    SourceRepository updateSourceRepositoryAttributes(String resourceId, SourceRepository sourcerepository) throws Exception;

}
