package org.kathra.resourcemanager.service;

import org.kathra.core.model.Group;
import java.util.List;

public interface GroupsService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new group
    * 
    * @param group Group object to be created (required)
    * @return Group
    */
    Group addGroup(Group group) throws Exception;

    /**
    * Delete a registered group
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteGroup(String resourceId) throws Exception;

    /**
    * Retrieve a specific group object
    * 
    * @param resourceId resource's id (required)
    * @return Group
    */
    Group getGroup(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible groups for authenticated user
    * 
    * @return List<Group>
    */
    List<Group> getGroups() throws Exception;

    /**
    * Fully update a registered group
    * 
    * @param resourceId resource's id (required)
    * @param group Group object to be updated (required)
    * @return Group
    */
    Group updateGroup(String resourceId, Group group) throws Exception;

    /**
    * Partially update a registered group
    * 
    * @param resourceId resource's id (required)
    * @param group Group object to be updated (required)
    * @return Group
    */
    Group updateGroupAttributes(String resourceId, Group group) throws Exception;

}
