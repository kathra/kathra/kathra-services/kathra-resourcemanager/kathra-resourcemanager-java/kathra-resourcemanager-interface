package org.kathra.resourcemanager.service;

import org.kathra.core.model.Library;
import java.util.List;

public interface LibrariesService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new library
    * 
    * @param library Library object to be created (required)
    * @return Library
    */
    Library addLibrary(Library library) throws Exception;

    /**
    * Delete a registered library
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteLibrary(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible libraries for authenticated user
    * 
    * @return List<Library>
    */
    List<Library> getLibraries() throws Exception;

    /**
    * Retrieve a specific library object
    * 
    * @param resourceId resource's id (required)
    * @return Library
    */
    Library getLibrary(String resourceId) throws Exception;

    /**
    * Fully update a registered library
    * 
    * @param resourceId resource's id (required)
    * @param library Library object to be updated (required)
    * @return Library
    */
    Library updateLibrary(String resourceId, Library library) throws Exception;

    /**
    * Partially update a registered library
    * 
    * @param resourceId resource's id (required)
    * @param library Library object to be updated (required)
    * @return Library
    */
    Library updateLibraryAttributes(String resourceId, Library library) throws Exception;

}
