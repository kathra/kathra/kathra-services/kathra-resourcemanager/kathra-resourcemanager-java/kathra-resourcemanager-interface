package org.kathra.resourcemanager.service;

import org.kathra.core.model.User;
import java.util.List;

public interface UsersService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new user
    * 
    * @param user User object to be created (required)
    * @return User
    */
    User addUser(User user) throws Exception;

    /**
    * Delete a registered user
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteUser(String resourceId) throws Exception;

    /**
    * Retrieve a specific user object
    * 
    * @param resourceId resource's id (required)
    * @return User
    */
    User getUser(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible users for authenticated user
    * 
    * @return List<User>
    */
    List<User> getUsers() throws Exception;

    /**
    * Fully update a registered user
    * 
    * @param resourceId resource's id (required)
    * @param user User object to be updated (required)
    * @return User
    */
    User updateUser(String resourceId, User user) throws Exception;

    /**
    * Partially update a registered user
    * 
    * @param resourceId resource's id (required)
    * @param user User object to be updated (required)
    * @return User
    */
    User updateUserAttributes(String resourceId, User user) throws Exception;

}
