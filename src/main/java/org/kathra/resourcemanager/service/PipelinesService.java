package org.kathra.resourcemanager.service;

import org.kathra.core.model.Pipeline;
import java.util.List;

public interface PipelinesService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new pipeline
    * 
    * @param pipeline Pipeline object to be created (required)
    * @return Pipeline
    */
    Pipeline addPipeline(Pipeline pipeline) throws Exception;

    /**
    * Delete a registered pipeline
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deletePipeline(String resourceId) throws Exception;

    /**
    * Retrieve a specific pipeline object
    * 
    * @param resourceId resource's id (required)
    * @return Pipeline
    */
    Pipeline getPipeline(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible pipelines for authenticated user
    * 
    * @return List<Pipeline>
    */
    List<Pipeline> getPipelines() throws Exception;

    /**
    * Fully update a registered pipeline
    * 
    * @param resourceId resource's id (required)
    * @param pipeline Pipeline object to be updated (required)
    * @return Pipeline
    */
    Pipeline updatePipeline(String resourceId, Pipeline pipeline) throws Exception;

    /**
    * Partially update a registered pipeline
    * 
    * @param resourceId resource's id (required)
    * @param pipeline Pipeline object to be updated (required)
    * @return Pipeline
    */
    Pipeline updatePipelineAttributes(String resourceId, Pipeline pipeline) throws Exception;

}
