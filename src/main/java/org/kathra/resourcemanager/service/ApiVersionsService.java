package org.kathra.resourcemanager.service;

import org.kathra.core.model.ApiVersion;
import java.util.List;

public interface ApiVersionsService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new apiversion
    * 
    * @param apiversion ApiVersion object to be created (required)
    * @return ApiVersion
    */
    ApiVersion addApiVersion(ApiVersion apiversion) throws Exception;

    /**
    * Delete a registered apiversion
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteApiVersion(String resourceId) throws Exception;

    /**
    * Retrieve a specific apiversion object
    * 
    * @param resourceId resource's id (required)
    * @return ApiVersion
    */
    ApiVersion getApiVersion(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible apiversions for authenticated user
    * 
    * @return List<ApiVersion>
    */
    List<ApiVersion> getApiVersions() throws Exception;

    /**
    * Fully update a registered apiversion
    * 
    * @param resourceId resource's id (required)
    * @param apiversion ApiVersion object to be updated (required)
    * @return ApiVersion
    */
    ApiVersion updateApiVersion(String resourceId, ApiVersion apiversion) throws Exception;

    /**
    * Partially update a registered apiversion
    * 
    * @param resourceId resource's id (required)
    * @param apiversion ApiVersion object to be updated (required)
    * @return ApiVersion
    */
    ApiVersion updateApiVersionAttributes(String resourceId, ApiVersion apiversion) throws Exception;

}
