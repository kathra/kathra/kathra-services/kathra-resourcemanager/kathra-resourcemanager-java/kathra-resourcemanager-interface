package org.kathra.resourcemanager.service;

import org.kathra.core.model.Implementation;
import java.util.List;

public interface ImplementationsService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new implementation
    * 
    * @param implementation Implementation object to be created (required)
    * @return Implementation
    */
    Implementation addImplementation(Implementation implementation) throws Exception;

    /**
    * Delete a registered implementation
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteImplementation(String resourceId) throws Exception;

    /**
    * Retrieve a specific implementation object
    * 
    * @param resourceId resource's id (required)
    * @return Implementation
    */
    Implementation getImplementation(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible implementations for authenticated user
    * 
    * @return List<Implementation>
    */
    List<Implementation> getImplementations() throws Exception;

    /**
    * Fully update a registered implementation
    * 
    * @param resourceId resource's id (required)
    * @param implementation Implementation object to be updated (required)
    * @return Implementation
    */
    Implementation updateImplementation(String resourceId, Implementation implementation) throws Exception;

    /**
    * Partially update a registered implementation
    * 
    * @param resourceId resource's id (required)
    * @param implementation Implementation object to be updated (required)
    * @return Implementation
    */
    Implementation updateImplementationAttributes(String resourceId, Implementation implementation) throws Exception;

}
