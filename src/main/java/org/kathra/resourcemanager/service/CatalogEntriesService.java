package org.kathra.resourcemanager.service;

import org.kathra.core.model.CatalogEntry;
import java.util.List;

public interface CatalogEntriesService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new catalogentry
    * 
    * @param catalogentry CatalogEntry object to be created (required)
    * @return CatalogEntry
    */
    CatalogEntry addCatalogEntry(CatalogEntry catalogentry) throws Exception;

    /**
    * Delete a registered catalogentry
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteCatalogEntry(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible catalogentries for authenticated user
    * 
    * @return List<CatalogEntry>
    */
    List<CatalogEntry> getCatalogEntries() throws Exception;

    /**
    * Retrieve a specific catalogentry object
    * 
    * @param resourceId resource's id (required)
    * @return CatalogEntry
    */
    CatalogEntry getCatalogEntry(String resourceId) throws Exception;

    /**
    * Fully update a registered catalogentry
    * 
    * @param resourceId resource's id (required)
    * @param catalogentry CatalogEntry object to be updated (required)
    * @return CatalogEntry
    */
    CatalogEntry updateCatalogEntry(String resourceId, CatalogEntry catalogentry) throws Exception;

    /**
    * Partially update a registered catalogentry
    * 
    * @param resourceId resource's id (required)
    * @param catalogentry CatalogEntry object to be updated (required)
    * @return CatalogEntry
    */
    CatalogEntry updateCatalogEntryAttributes(String resourceId, CatalogEntry catalogentry) throws Exception;

}
