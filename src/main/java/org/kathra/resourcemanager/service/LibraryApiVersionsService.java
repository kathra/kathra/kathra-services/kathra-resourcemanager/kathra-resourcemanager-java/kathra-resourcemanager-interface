package org.kathra.resourcemanager.service;

import org.kathra.core.model.LibraryApiVersion;
import java.util.List;

public interface LibraryApiVersionsService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new libraryapiversion
    * 
    * @param libraryapiversion LibraryApiVersion object to be created (required)
    * @return LibraryApiVersion
    */
    LibraryApiVersion addLibraryApiVersion(LibraryApiVersion libraryapiversion) throws Exception;

    /**
    * Delete a registered libraryapiversion
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteLibraryApiVersion(String resourceId) throws Exception;

    /**
    * Retrieve a specific libraryapiversion object
    * 
    * @param resourceId resource's id (required)
    * @return LibraryApiVersion
    */
    LibraryApiVersion getLibraryApiVersion(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible libraryapiversions for authenticated user
    * 
    * @return List<LibraryApiVersion>
    */
    List<LibraryApiVersion> getLibraryApiVersions() throws Exception;

    /**
    * Fully update a registered libraryapiversion
    * 
    * @param resourceId resource's id (required)
    * @param libraryapiversion LibraryApiVersion object to be updated (required)
    * @return LibraryApiVersion
    */
    LibraryApiVersion updateLibraryApiVersion(String resourceId, LibraryApiVersion libraryapiversion) throws Exception;

    /**
    * Partially update a registered libraryapiversion
    * 
    * @param resourceId resource's id (required)
    * @param libraryapiversion LibraryApiVersion object to be updated (required)
    * @return LibraryApiVersion
    */
    LibraryApiVersion updateLibraryApiVersionAttributes(String resourceId, LibraryApiVersion libraryapiversion) throws Exception;

}
