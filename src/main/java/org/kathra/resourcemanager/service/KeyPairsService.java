package org.kathra.resourcemanager.service;

import org.kathra.core.model.KeyPair;
import java.util.List;

public interface KeyPairsService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new keypair
    * 
    * @param keypair KeyPair object to be created (required)
    * @return KeyPair
    */
    KeyPair addKeyPair(KeyPair keypair) throws Exception;

    /**
    * Delete a registered keypair
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteKeyPair(String resourceId) throws Exception;

    /**
    * Retrieve a specific keypair object
    * 
    * @param resourceId resource's id (required)
    * @return KeyPair
    */
    KeyPair getKeyPair(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible keypairs for authenticated user
    * 
    * @return List<KeyPair>
    */
    List<KeyPair> getKeyPairs() throws Exception;

    /**
    * Fully update a registered keypair
    * 
    * @param resourceId resource's id (required)
    * @param keypair KeyPair object to be updated (required)
    * @return KeyPair
    */
    KeyPair updateKeyPair(String resourceId, KeyPair keypair) throws Exception;

    /**
    * Partially update a registered keypair
    * 
    * @param resourceId resource's id (required)
    * @param keypair KeyPair object to be updated (required)
    * @return KeyPair
    */
    KeyPair updateKeyPairAttributes(String resourceId, KeyPair keypair) throws Exception;

}
