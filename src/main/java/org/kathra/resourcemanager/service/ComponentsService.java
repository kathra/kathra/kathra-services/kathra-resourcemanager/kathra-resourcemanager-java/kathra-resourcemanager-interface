package org.kathra.resourcemanager.service;

import org.kathra.core.model.Component;
import org.kathra.core.model.Implementation;
import java.util.List;

public interface ComponentsService extends org.kathra.iface.KathraAuthRequestHandler {

    /**
    * Add a new component
    * 
    * @param component Component object to be created (required)
    * @param groupPath group's path (required)
    * @return Component
    */
    Component addComponent(Component component, String groupPath) throws Exception;

    /**
    * Delete a registered component
    * 
    * @param resourceId resource's id (required)
    * @return String
    */
    String deleteComponent(String resourceId) throws Exception;

    /**
    * Retrieve a specific component object
    * 
    * @param resourceId resource's id (required)
    * @return Component
    */
    Component getComponent(String resourceId) throws Exception;

    /**
    * Retrieve a list of accessible components for authenticated user
    * 
    * @return List<Component>
    */
    List<Component> getComponents() throws Exception;

    /**
    * Retrieve a list of accessible implementations for specific component and authenticated user
    * 
    * @param resourceId resource's id (required)
    * @return List<Implementation>
    */
    List<Implementation> getImplementationsComponent(String resourceId) throws Exception;

    /**
    * Fully update a registered component
    * 
    * @param resourceId resource's id (required)
    * @param component Component object to be updated (required)
    * @return Component
    */
    Component updateComponent(String resourceId, Component component) throws Exception;

    /**
    * Partially update a registered component
    * 
    * @param resourceId resource's id (required)
    * @param component Component object to be updated (required)
    * @return Component
    */
    Component updateComponentAttributes(String resourceId, Component component) throws Exception;

}
